import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

// Definicion de la camara
const fov = 75; // Campo de vision
const aspect = window.innerWidth / window.innerHeight; // Relacion de aspecto
const near = 0.1; // Distancia de recorte cercana
const far = 1000; // Distancia de recorte lejana
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
camera.position.z = 5;
//camera.position.y = 10;

// Creacion de la escena
const scene = new THREE.Scene();

// Creacion de geometria
const boxWidth = 2;
const boxHeight = 2;
const boxDepth = 2;
const geometry = new THREE.BoxGeometry(boxWidth, boxHeight, boxDepth);

const material = new THREE.MeshBasicMaterial({
  color: 0x44aa88,
  wireframe: true,
});

/*
const material = new THREE.MeshPhongMaterial({
  color: 0x008000,
  flatShading: true,
});

const color = 0xFFFFFF;
const intensity = 1;
const light = new THREE.DirectionalLight(color, intensity);
light.position.set(20, 10, 10);
light.target.position.set(0, 0, 0);
scene.add(light);
scene.add(light.target);
*/

// Creacion de mesh
const cube = new THREE.Mesh(geometry, material);
cube.rotation.y = 1.2;

scene.add(cube);

const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);

// Agrega la escena al DOM
document.getElementById("app").appendChild(renderer.domElement);

// Crea un control de orbita
const controls = new OrbitControls(camera, renderer.domElement);

function render(radians) {
  radians *= 0.001; // convert time to seconds

  cube.rotation.y = radians;
  //cube.rotation.x = time;

  renderer.render(scene, camera);
  controls.update();
  requestAnimationFrame(render);
}

requestAnimationFrame(render);
