import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

// Crea la scena
const scene = new THREE.Scene();

// Crea la camara
const camera = new THREE.PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);
camera.position.set(-50, 40, 30);

// Crear el renderer
const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.shadowMap.enabled = true;

// Agrega Spot Light
const spotLight = new THREE.SpotLight("white");
spotLight.position.set(50, 50, 50);
spotLight.castShadow = true;
//spotLight.shadow.mapSize = new THREE.Vector2(1024, 1024);
//spotLight.shadow.camera.far = 130;
//spotLight.shadow.camera.near = 40;
scene.add(spotLight);

// Spot Light Helper
const spotLightHelper = new THREE.SpotLightHelper( spotLight );
scene.add( spotLightHelper );

// Agrega Helper para mostrar los ejes de coordenadas 
const axesHelper = new THREE.AxesHelper( 100 );
scene.add( axesHelper );

// Agrega plano
const planeGeometry = new THREE.PlaneGeometry(70, 30);
const planeMaterial = new THREE.MeshLambertMaterial({ color: "white" });
const plane = new THREE.Mesh(planeGeometry, planeMaterial);
plane.position.set(0, 0, 0);
plane.rotation.x = -0.5 * Math.PI;
plane.receiveShadow = true;
scene.add(plane);

// Agrega Sphere
const sphereGeometry = new THREE.SphereGeometry(5, 20, 20);
const sphereMaterial = new THREE.MeshLambertMaterial({
  color: "yellow",
});
const sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
sphere.position.set(20, 4, 2);
sphere.castShadow = true;
scene.add(sphere);

// Agrega escena al DOM
document.getElementById("app").appendChild(renderer.domElement);

// Crea un control de orbita
const controls = new OrbitControls(camera, renderer.domElement);

let step = 0;
const speed = 0.05;
function render() {
  step += speed;
  sphere.position.x = 20 * Math.cos(step);
  sphere.position.y = 2 + 10 * Math.abs(Math.sin(step));
  controls.update(); 
  window.requestAnimationFrame(render);
  renderer.render(scene, camera);
}

render();
